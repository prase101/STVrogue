﻿using NUnit.Framework;
using STVrogue.GameLogic;

using System;
namespace NUnitTests
{
    [TestFixture]
    public class Test_SavingLoadingGameConfig
    {
    
        [Test]
        public void Test_Saving_and_Loading_GameConfig()
        {
            GameConfiguration config = new GameConfiguration();
            config.numberOfRooms = 10;
            config.dungeonShape = DungeonShapeType.RANDOMshape;
            config.difficultyMode = DifficultyMode.ELITEmode;
            config.SaveToFile("test_rogueconfig.txt");
            GameConfiguration config2 = new GameConfiguration("test_rogueconfig.txt");
            Assert.AreEqual(config2.numberOfRooms,config.numberOfRooms); 
            Assert.AreEqual(config2.dungeonShape,config.dungeonShape); 
            Assert.AreEqual(config2.difficultyMode,config.difficultyMode);
        }
    }
}
