﻿using System;
using STVrogue.Utils;

namespace STVrogue.GameLogic
{
    /// <summary>
    /// This class represents the whole game-state of STVRogue. It also contains
    /// the method <see cref="Update"/> that performs a single turn update to
    /// the game state.
    ///
    /// The game's main-loop is put separately in the class <see cref="GameRunner"/>.
    /// 
    /// <para></para>
    /// The main methods for this class are:
    ///
    /// <list type="bullet">
    ///
    /// <item> The constructor, for creating an instance of this Game, with a 
    /// dungeon according to a given configuration. </item>
    /// 
    /// <item> The method <see cref="Update"/> to do a single turn update. This is called
    /// from the mainloop in <see cref="GameRunner"/>. </item>
    /// 
    /// <item> The method <see cref="Flee"/> for you to program the logic of fleeing creatures.</item>
    ///
    /// </list>
    /// </summary>
    public class Game 
    {
        #region fields and pro
        
        public GameConfiguration Config { get; }
        
        public Player Player { get; }
        
        public Dungeon Dungeon { get; }

        public bool Gameover { get; } = false;
        
        /// <summary>
        /// To count the number of passed turns. 
        /// </summary>
        public int TurnNumber { get; private set; } = 0;

        
        /// <summary>
        /// A <see cref="GameConsole"/> provides a text-based Console. You can print strings on this console,
        /// or read strings from it. Using this to handle your text I/O.
        /// NOTE: Don't read and write directly to System's Console.
        /// </summary>
        public GameConsole GameConsole { get; set;  }

        /// <summary>
        /// A random generator you can use for making random decisions. The type
        /// is intentionally set to to be an instance of <see cref="IRandomGenerator"/>
        /// to prevent you from directly using <see cref="Random"/>.
        /// When testing the game you need a setup where all your random generators
        /// behave deterministically, to avoid your testing to become flaky.
        /// The code below will use an instance of <see cref="RandomGenerator"/>,
        /// which is NOT deterministic. 
        /// Check out the other implementation of <see cref="IRandomGenerator"/>, namely
        /// <see cref="STVControlledRandom"/>, or else write your own implementation.
        /// </summary>
        IRandomGenerator rnd = new RandomGenerator();
        //IRandomGenerator rnd = new STVControlledRandom();
        
        #endregion
        
        public Game()
        {
            Player = new Player("0", "Bagginssess");
        }

        /// <summary>
        /// Try to create an instance of Game satisfying the specified configuration.
        /// It should throw an exception if it does not manage to generate a dungeon
        /// satisfying the configuration.
        /// </summary>
        public Game(GameConfiguration conf) : this()
        {
            // A dummy implementation that ignores the configuration. You should fix this
            // by implementing this constructor according to its description in the Project
            // Document.
            Config = conf;
            STVControlledRandom.SetSeed(conf.rndSeed);
            Console.Error.WriteLine(">>> Creating an instance of Game, but ignoring the passed configuration. Fix this.");
        }
        

        /// <summary>
        /// Cause a creature to flee a combat. This will take the creature to a neighboring
        /// room. This should not breach the capacity of that room. Note that fleeing a
        /// combat is not always possible --see the Project Document.
        /// The method returns true if fleeing was successful, else false.
        /// </summary>
        public bool Flee(Creature c)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Perform a single turn-update on the game. In every turn, each creature
        /// is allowed to do one action. The player does and specified in the argument
        /// of this method. A monster can either do nothing, move, attack, or flee.
        /// See the Project Document that defines when these are possible.
        /// The order in which creatures execute their actions is left for you to decide.
        /// </summary>
        public void Update(Command playerAction)
        {
            GameConsole.WriteLines("", "** "  + Player.Name + " " + playerAction);
            switch (playerAction.Name)
            {
                case CommandType.ATTACK :
                    GameConsole.WriteLines("      Clang! Wooosh. WHACK!");
                    break;
                case CommandType.FLEE:
                    GameConsole.WriteLines("      We knew you are a coward.");
                    break;
                case CommandType.DoNOTHING:
                    GameConsole.WriteLines("      Lazy. Start working!");
                    break;
            }
            TurnNumber++;
        }
    }
}
