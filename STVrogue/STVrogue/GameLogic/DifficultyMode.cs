using System;

namespace STVrogue.GameLogic
{
    /// <summary>
    /// Specifying three types of difficulty level for playing STVRogue.
    /// </summary>
    [Serializable()]
    public enum DifficultyMode {
        NEWBIEmode,
        NORMALmode,
        ELITEmode
    }
}