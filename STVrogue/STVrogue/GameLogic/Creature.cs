﻿using System;
using System.Collections.Generic;


namespace STVrogue.GameLogic
{
    public class Creature : GameEntity
    {
        #region Fields and properties
        
        public string Name { get;  }
        public int HpMax { get;  }
        
        // current HP, should never exceed HPmax
        public int Hp { get; set; } = 1;    
        public bool Alive => Hp > 0;

        public Room Location { get; set; }

        public int AttackRating { get ; set; }
        
        #endregion
        
        public Creature(string id, String name) : base(id)
        {
            Name = name;
            // you need to decide how to initialize the other attributes
        }

        public Creature(string id, int hp, int ar) : base(id)
        {
            if(id==null || hp<=0 || ar<=0) 
                throw new ArgumentException();
            Hp = hp;
            HpMax = hp;
            AttackRating = ar;
        }
        
        /// <summary>
        /// Move this creature to the given room. This is only allowed if r
        /// is a neighboring room of the creature's current location. Also
        /// keep in mind that rooms have capacity.
        ///
        /// <para></para>
        /// NOTE: override this in respective subclasses.
        /// </summary>
        public virtual void Move(Room r)
        {
            if (!Location.Neighbors.Contains(r))
            {
                throw new ArgumentException();
            }
        }
        
        /// <summary>
        /// Attack the given foe. This is only possible if this creature is alive and
        /// if the foe is in the same room as this creature.
        /// The code below provides a base implementation of this method. You may have
        /// to override this for Player.
        /// </summary>
        public virtual void Attack(Creature foe)
        {
            if (!Alive 
                || Location != foe.Location 
                || !foe.Alive)
            {
                throw new ArgumentException();
            }
            foe.Hp = Math.Max(0,foe.Hp - AttackRating);
        }
       
    }

    /// <summary>
    /// Representing monsters.... you know, those scary things you don't want
    /// to mess with.
    /// </summary>
    public class Monster : Creature
    {
        public Monster(String id, String name) : base(id,name)
        {
        }
        
        public override void Move(Room r)
        {
            base.Move(r);
            if (r.NumberOfMonsters > r.Capacity) // kutu note
            {
                throw new ArgumentException();
            }
            r.Creatures.Add(this);
            Location.Creatures.Remove(this);
            Location = r;
        }
    }

    public class Player : Creature
    {
        /* kill point */
        int kp = 0;
        List<Item> bag = new List<Item>();

        /// <summary>
        /// True if the player is enraged. The player enters this state whenever it uses a rage potion.
        /// The effect last for 5 turns including the turn when the potion is used.
        /// </summary>
        bool enraged = false;

        public Player(string id, string name) : base(id,name)
        {
            // you need to decide how to initialize the other attributes
            // for example:
            Hp = 100; // etc...
        }

        #region getters setters
        public int Kp
        {
            get => kp;
            set => kp = value;
        }

        public List<Item> Bag
        {
            get => bag;
            set => bag = value;
        }
        
        public bool Enraged
        {
            get => enraged;
            set => enraged = value;
        }
        #endregion

        public override void Move(Room r)
        {
            base.Move(r);
            Location = r;
        }
        
        /// <summary>
        /// Use the given item. We also pass the current turn-number at which
        /// this action happens.
        /// </summary>
        public void Use(int turnNr, Item i)
        {
            throw new NotImplementedException();
        }
    }
    
}
