﻿using System;
namespace STVrogue.GameLogic
{
    public class Item : GameEntity
    {
        public Item(String id) : base(id){ }
        
    }

    public class HealingPotion : Item
    {
        /* it can heal this many HP */
        public int HealValue { get;  }

        public HealingPotion(String id, int heal) : base(id)
        {
            HealValue = heal;
        }
        
    }

    public class RagePotion : Item
    {
        public RagePotion(String id) : base(id){ }
        
    }

}
