namespace STVrogue.TestInfrastructure
{
    /// <summary>
    /// NOTE: This is for Part-2 of the project.
    ///
    /// <para/>
    /// A GamePlay represents a recorded run of a game, which can be replayed
    /// again to check some correctness property. The method <see cref="Satisfies"/>
    /// runs a replay, and as it goes checks the property you give to it, whether
    /// the property is satisfied by the run, or violated.
    ///
    /// <para></para>Being an interface, here we only define
    /// the signature of the methods relevant for doing a replay. To actually do a replay
    /// you need to implement this interface.
    /// The class <see cref="STVrogue.GameRunner"/> has been designed to implement
    /// this interface.
    /// 
    ///  
    /// </summary>
    /// <typeparam name="GameState"> The type of the state of the game
    /// whose runs we want to replay.
    /// </typeparam>
    public interface GamePlay<GameState>
    {
        /// <summary>
        /// Reset the recorded game-play.
        /// <para></para>
        /// NOTE: reset your random generators too, if the replays
        /// might call them.
        /// </summary>
        public void ResetReplayState();

        /// <summary>
        /// Return the id/name of this gameplay.
        /// </summary>
        public string ReplayId();

        /// <summary>
        /// Replay this recorded gameplay to check the given correctness property.
        /// It returns Valid if the gameplay satisfied the property, or Invalid if the property
        /// is violated.
        /// It returns Inconclusive if neither Valid nor Invalid can be concluded.
        /// </summary>
        /// <param name="phi">
        /// The correctness property to check. It is exprrssed as
        /// an instance of <see cref="TemporalProperty"/>.
        /// </param>
        public Judgement Satisfies(TemporalProperty<GameState> phi);
    }
}