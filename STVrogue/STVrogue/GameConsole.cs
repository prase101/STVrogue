using System;
using System.Collections.Generic;

namespace STVrogue
{
    /// <summary>
    /// This class provides methods to interact with the console. That is, to send
    /// strings to be printed on the console, and to read input key or string from
    /// the console. 
    /// <para></para>
    /// NOTE: Use this class, or its subclasses, to do your game I/O with
    /// the console.
    /// Don't directly use <c>Console.Write</c> and <c>Console.Read</c>.
    /// </summary>
    public class GameConsole
    {
        /// <summary>
        /// Write one or more strings to the Console, separated by new-lines.
        /// </summary>
        public virtual void WriteLines(params string[] messages)
        {
            foreach (string msg in messages)
            {
                Console.WriteLine(msg);
            }
        }
        
        /// <summary>
        /// Read a key-board key pressed by the user. It returns a character representing
        /// the pressed key.
        /// </summary>
        public virtual char ReadKey()
        {
            char c = Console.ReadKey().KeyChar;
            return c;
        }
        
        /// <summary>
        /// Read a line of string that the user entered in the Console. The user should
        /// hit the ENTER-key to close the line.
        /// </summary>
        public virtual  string ReadLine()
        {
            string line = Console.ReadLine();
            return line;
        }
    }
    
    /// <summary>
    /// This is a variation of <see cref="GameConsole"/> that provides 'instrumentation'. It will store
    /// strings sent to the Console in a list. It can store all, or only the strings from
    /// the last <see cref="WriteLines"/>. The latter is the default.
    /// <para></para>
    /// This Console also stores all keys/strings that the user entered with <see cref="ReadKey"/>
    /// or <see cref="ReadLine"/>.
    /// </summary>
    public class InstrumentedGameConsole : GameConsole
    {
        /// <summary>
        /// If this flag is set to false this GameConsole will remember all strings written to it via
        /// <see cref="WriteLines"/>. Else, if this flag is set to true, then only strings
        /// sent by the last <see cref="WriteLines"/> will be remembered.
        ///
        /// <para></para>The default value is true.
        /// </summary>
        bool wipeMsgOutAtEveryWriteLines = true;
        
        /// <summary>
        /// Strings sent to this Console will be remembered here. See also <see cref="wipeMsgOutAtEveryWriteLines"/>.
        /// </summary>
        List<string> msgOut = new List<string>();
        
        /// <summary>
        /// Strings/keys read from this Console will be remembered here.
        /// </summary>
        List<string> msgIn = new List<string>();

        /// <summary>
        /// Get the recorded strings sent to this Console.
        /// </summary>
        public List<string> MsgOut => msgOut;
        
        /// <summary>
        /// Get the recorded keys/strings that the user entered to this Console.
        /// </summary>
        public List<string> MsgIn => msgIn;

        /// <summary>
        /// If set to true (default) this Console will only remember the strings written to
        /// this Console since the last <see cref="WriteLines"/>. If set to false, then
        /// all strings written to this console will be remembered.
        /// </summary>
        public bool WipeMsgOutAtEveryWriteLines
        {
            get => wipeMsgOutAtEveryWriteLines;
            set => wipeMsgOutAtEveryWriteLines = value;
        }
        
        public override void WriteLines(params string[] messages)
        {
            if (wipeMsgOutAtEveryWriteLines) 
                msgOut.Clear();
            foreach (string msg in messages)
            {
                msgOut.Add(msg);
            }
            base.WriteLines(messages);
        }
        
        public override char ReadKey()
        {
            char c = base.ReadKey();
            msgIn.Add(c.ToString());
            return c;
        }
        
        public override string ReadLine()
        {
            string line = base.ReadLine(); 
            msgIn.Add(line);
            return line;
        }
        
    }

    /// <summary>
    /// An implementation of <see cref="GameConsole"/> that allows user-inputs to be
    /// replayed. The implementation is not finished; finish it.
    ///
    /// <para></para>
    /// Rather than reading inputs live from the user, this class reads input from
    /// list of strings representing recorded inputs. You can for example use <see cref="InstrumentedGameConsole"/>
    /// to obtain such a recording.
    ///
    /// <para></para>
    /// This list of inputs is kept in <see cref="InstrumentedGameConsole.msgIn"/>. You can set values into this list
    /// using the method <see cref="SeedInputs"/>.
    /// When invoked, the methods <see cref="ReadKey"/> and <see cref="ReadLine"/> will read
    /// from this list, one string at a time, rather than from user's live input.
    /// </summary>
    public class ReplayGameConsole : InstrumentedGameConsole
    {
        
        /// <summary>
        /// The name to the recorded inputs stored in <see cref="InstrumentedGameConsole.msgIn"/>.
        /// </summary>
        public string Name { get; set; } = "My-Play";

        /// <summary>
        /// Reset this Console, so that it can read from the start of <see cref="InstrumentedGameConsole.msgIn"/> again. 
        /// </summary>
        public void Reset()
        {
            throw  new NotImplementedException();
        }
        
        /// <summary>
        /// Read the next string from the list <see cref="InstrumentedGameConsole.msgIn"/>. Return
        /// the first character of this string.
        /// </summary>
        public override char ReadKey()
        {
            throw  new NotImplementedException();
        }
        
        /// <summary>
        /// Read the next string from the list <see cref="InstrumentedGameConsole.msgIn"/>. 
        /// </summary>
        public override string ReadLine()
        {
            throw  new NotImplementedException();
        }
        
        /// <summary>
        /// Put some values to <see cref="InstrumentedGameConsole.msgIn"/> to be replayed.
        /// </summary>
        public void SeedInputs(params string[] inputs)
        {
            foreach (string s in inputs)
            {
                MsgIn.Add(s);
            }
        }
        
    }
}